//
//  GeofenceManager.h
//  LocationSDK
//
//  Created by Monospect Yazılım Bilişim Hiz. Tic. Ltd. Şti. on 11/11/15.
//  Copyright © 2015 Vodafone Teknoloji A.Ş. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VLGeofenceManager;

@protocol VLGeofenceRegion <NSObject>

-(NSString*) getUniqueRegionIdentifier;
-(void) registerRegion:(id<VLGeofenceManager>)manager;
-(double)boundingCircleRadiusWithCenterLatitude:(double)latitude Longitude:(double)longitude;
-(id)getAttributes;
@end

@protocol VLGeofenceManagerDataSource <NSObject>
-(void) requestNearbyRegions:(id<VLGeofenceManager>)manager Latitude:(double)latitude Longitude:(double)longitude;
@end

typedef NS_ENUM(NSInteger, VLGeofenceEvent) {
    VLGeofenceEventEnter,
    VLGeofenceEventLeave
};

@protocol VLGeofenceManagerDelegate <NSObject>

@optional
-(void) geofenceManager:(id<VLGeofenceManager>)manager
didReceiveGeofenceEvent:(VLGeofenceEvent)event Region:(id<VLGeofenceRegion>)region;

@optional
-(void) geofenceManager:(id<VLGeofenceManager>)manager
       didFailWithError:(NSError*)error;

@end


@protocol VLGeofenceManager <NSObject>

@property (nonatomic, weak) id <VLGeofenceManagerDelegate> delegate;
@property (nonatomic, weak) id <VLGeofenceManagerDataSource> dataSource;

-(void) startMonitoring;
-(void) stopMonitoring;
-(void) setRegionsForMonitoring:(NSArray<id<VLGeofenceRegion>>*)regions;
-(void) registerRegion:(id<VLGeofenceRegion>)region Latitude:(double)latitude Longitude:(double)longitude Radius:(double)radius;
-(void) registerRegion:(id<VLGeofenceRegion>)region UUID:(NSString*)uuid Major:(NSNumber*)major Minor:(NSNumber*)minor;
@end