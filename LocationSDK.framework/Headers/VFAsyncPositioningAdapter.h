//
//  VFAsyncPositioningAdapter.h
//  MapView2
//
//  Created by can gocmenoglu on 06/08/15.
//  Copyright (c) 2015 Monospect. All rights reserved.
//


#import <Foundation/Foundation.h>

#import "VFIndoorLocation.h"

@protocol VFAsyncPositioningAdapter <NSObject>

-(void)calculateIndoorPosition:(NSArray*)measurementSnapshot Completion:(void (^)(VFIndoorLocation*))callback;

@end