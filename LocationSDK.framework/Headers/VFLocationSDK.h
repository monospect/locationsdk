//
//  VFLocationSDK.h
//  LocationSDKTest
//
//  Created by Monospect Yazılım Bilişim Hiz. Tic. Ltd. Şti. on 18/11/15.
//  Copyright © 2015 Vodafone Teknoloji A.Ş. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VLGeofenceManager.h"
#import "VFIndoorPositioningService.h"
#import "VFLocationAPI.h"

#define VF_SERVER_URL @"https://indoorapi.oksijen.com/indoor-server/"


/**
 This class provides methods for accessing all functions provided by the SDK.
 
 At the start of your program, usually in your `AppDelegate`'s `didFinishLaunchingWithOptions` method, initialize this class using your unique api key.
 
 Just by initializing this class, SDK will start monitoring user's location and notify Vodafone Location platform of user's location and geofence events.
 
 ## Providing client information
 
 You can provide device msisdn, imsi and custom codes to this class. These information will be attached to SDK events for further analysis by Vodafone. See `setUserInfoWithExtId` method for furher details.
 
 ## Geofencing
 
 Geofences are created using Vodafone Location platform web UI. Using the SDK you can catch these geofence events. Geofencing is started when this class is initialized by default.
 
 ## Indoor Positioning
 
 If you start indoor positioning using `startPositioning` method, if available, you will be notified of the device's indoor location. This technology uses indoor beacons setup using Vodafone Location web UI.
 
 If there are previously setup beacons are present, and device's bluetooth is ON, a periodic Location event will occur. You'll have access to a Location ID and (x,y) coordinates.
 
 @warning This is a singleton class, access all methods using sharedInstance. Methods are not thread-safe. Call them only on main thread.
 */
@interface VFLocationSDK : NSObject
 

/**
 @warning DO NOT USE. Used internally.
 */
@property (nonatomic, weak) id <VLGeofenceManagerDelegate> geofencingDelegate;

/**
 @warning DO NOT USE. Used internally.
 */
@property (nonatomic, weak) id <VLGeofenceManagerDataSource> geofencingDataSource;

/**
 @warning DO NOT USE. Used internally.
 */
@property(nonatomic) NSString* imsi;

/**
 @warning DO NOT USE. Used internally.
 */
@property(nonatomic) NSString* extId;

/**
 @warning DO NOT USE. Used internally.
 */
@property(nonatomic) NSString* extCode;

/**
 @warning DO NOT USE. Used internally.
 */
@property(nonatomic) NSString* msisdn;


/**
 Initialize singleton.

 @return Singleton instance
 */
+ (instancetype)sharedInstance;


/**
 Starts geofencing. Geofencing is started by default, works only if location permission is given. Bluetooth is required to be ON for bluetooth geofences.
 */
-(void)startGeofencing;
/**
 Stops geofencing.
 */
-(void)stopGeofencing;
/**
 Starts indoor positioning. Works only if the app is in foreground, user is given location permission and blutooth is ON.
 */
-(void)startPositioning;
/**
 Stops indoor positioning.
 */
-(void)stopPositioning;


/**
 You have to call this method on the singleton instance to start the SDK.

 @param apikey    Api key given by Vodafone
 @param completed This block is called when api is initialized.
 */
-(void)setupWithApiKey:(NSString*)apikey
            Completion:(void (^)(NSError*))completed;


/**
 Read api key.

 @return Api Key string.
 */
-(NSString*)getApiKey;


/**
 Set client information.

 @param extId   a custom external id, set by your application.
 @param imsi    imsi of device (if available)
 @param extCode a custom external code
 @param msisdn  msisdn of user
 */
-(void)setUserInfoWithExtId:(NSString*)extId Imsi:(NSString*)imsi ExtCode:(NSString*)extCode MSISDN:(NSString*)msisdn;

/**
 @warning DO NOT USE. Used internally.
 */
-(void)setVFGeofenceDelegate:(id<VLGeofenceManagerDelegate>)dlg;


/**
 Delete client information. This can be used, for example, when your user signs out.
 */
-(void)deleteUserInfo;

/**
 @warning DO NOT USE. Used internally.
 */
- (VFLocationAPI*)getAPI;

@end
