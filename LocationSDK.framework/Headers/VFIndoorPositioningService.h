//
//  VFIndoorPositioningService.h
//  MapView2
//
//  Created by can gocmenoglu on 06/08/15.
//  Copyright (c) 2015 Monospect. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VFAsyncPositioningAdapter.h"

extern NSString * const VFIPSDidCalculateLocation;
extern NSString * const VFIPSIndoorLocationKey;

@interface VFIndoorPositioningService : NSObject

-(instancetype)initWithPositioningAdapter:(id<VFAsyncPositioningAdapter>)adapter;

@end
