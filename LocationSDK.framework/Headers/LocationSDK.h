//
//  LocationSDK.h
//  LocationSDK
//
//  Created by Monospect Yazılım Bilişim Hiz. Tic. Ltd. Şti. on 09/01/17.
//  Copyright © 2017 Vodafone. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 SDK version number
 */
FOUNDATION_EXPORT double LocationSDKVersionNumber;

/**
 SDK version string
 */
FOUNDATION_EXPORT const unsigned char LocationSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <LocationSDK/PublicHeader.h>


#import <LocationSDK/VFLocationSDK.h>
