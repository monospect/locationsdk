//
//  VFLocationAPI.h
//  LocationSDK
//
//  Created by Monospect Yazılım Bilişim Hiz. Tic. Ltd. Şti. on 09/01/17.
//  Copyright © 2017 Vodafone. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 `VFLocationAPI` is a convenience class for contacting Vodafone Location Platform API.
 
 
 @warning Third-party developers should not use VFLocationAPI directly. Use `VFLocationSDK` instead.
 */
@interface VFLocationAPI : NSObject


/**
 Create new instance using a Vodafone Location API key.

 @param apikey API key provided by Vodafone

 @return a new instance of VFLocationAPI
 */
- (instancetype)initWithApiKey:(NSString*)apikey;


/**
 Requests Vendor beacon uuids from Location API

 @param completed Block to be called with an array of Vendor Beacon UUIDs after completion
 */
- (void)requestVendorUUIDsWithCompletion:(void (^)(NSArray*))completed;


/**
 Requests nearby geofences from Location API.

 @param N         number of geofences to request
 @param lat       current latitude of device
 @param lng       current longitude of device
 @param extId     external id for analysis
 @param imsi      imsi of device
 @param extCode   external code for analysis
 @param msisdn    msisdn of device
 @param completed block called on completion with an array of geofences, nil if error.
 */
- (void)requestNearestNGeofencesWithN:(int)N
                              Lat:(double)lat Lng:(double)lng
                            ExtId:(NSString*)extId Imsi:(NSString*)imsi ExtCode:(NSString*)extCode MSISDN:(NSString*)msisdn
                       Completion:(void (^)(NSArray*))completed;


/**
 Requests beacons using a list of beacon ids.

 @param wlanIds   array of beacon ids (NSNumber)
 @param completed block called on completion with an array of beacon information
 */
- (void)requestWlansWithIdList:(NSArray*)wlanIds Completion:(void (^)(NSArray*))completed;


/**
 Register a geofence event to remote API.

 @param geofenceId Id of geofence
 @param extId      external id
 @param imsi       imsi of device
 @param extCode    external code
 @param msisdn     msisdn of device
 */
- (void)registerGeofenceEventWithId:(NSNumber*)geofenceId
                              ExtId:(NSString*)extId Imsi:(NSString*)imsi
                            ExtCode:(NSString*)extCode MSISDN:(NSString*)msisdn;


/**
 Request indoor beacons using a beacon uuid,major,minor as hint. This will return all nearby indoor beacons for further processing.

 @param uuid           hint uuid
 @param major          hint major
 @param minor          hint minor
 @param onReceiveWlans block to be called with a list of indoor beacons. this may be called multiple times for each location.
 */
- (void)requestWlansWithBeaconNeighborUUID:(NSString*)uuid Major:(long)major Minor:(long)minor Completion:(void (^)(long, NSArray*))onReceiveWlans;
@end
