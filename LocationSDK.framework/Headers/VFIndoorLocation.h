//
//  VFIndoorLocation.h
//  MapView2
//
//  Created by can gocmenoglu on 01/08/15.
//  Copyright (c) 2015 Monospect. All rights reserved.
//

#import <Foundation/Foundation.h>

@import GLKit;

@interface VFIndoorLocation : NSObject

@property(readonly) long locationId;
@property(readonly) GLKVector2 position;

-(instancetype)initWithIndoorLocation:(VFIndoorLocation*)location;
-(instancetype)initWithLocationId:(long)locationId X:(double)x Y:(double)y;
-(instancetype)initWithLocationId:(long)locationId Position:(GLKVector2)position;

-(VFIndoorLocation*)locationByMoving:(GLKVector2)delta;
-(double)distanceTo:(VFIndoorLocation*)rhs;

@end
