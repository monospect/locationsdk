Pod::Spec.new do |spec|
  spec.name         = 'LocationSDK'
  spec.version      = '1.0.0'
  spec.license      = { :type => 'BSD' }
  spec.homepage     = 'https://indoor.oksijen.com'
  spec.authors      = { 'Can Göçmenoğlu' => 'can@monospect.com' }
  spec.summary      = 'Vodafone Location SDK'

  spec.platform = :ios, '8.0'
  spec.ios.deployment_target = '8.0'

  spec.source = { :git => "https://gitlab.com/monospect/locationsdk.git",
                  :tag => spec.version.to_s }
  spec.vendored_frameworks = 'LocationSDK.framework'
  spec.preserve_paths = 'LocationSDK.framework'
  spec.framework  = "LocationSDK"
  
end